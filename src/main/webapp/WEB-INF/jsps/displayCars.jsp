<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page isELIgnored="false"%>

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Display Cars</title>
</head>
<body>

<h2>cars:</h2>
<table>
<tr>
<th>id</th>
<th>vin</th>
<th>brand</th>
<th>model</th>
<th>type</th>
<th>year</th>
</tr>

<c:forEach items="${cars}" var="car">
<tr>
<td>${car.id}</td>
<td>${car.vin}</td>
<td>${car.brand}</td>
<td>${car.model}</td>
<td>${car.type}</td>
<td>${car.year}</td>
<td><a href="deleteCar?id=${car.id}">delete</a></td>
<td><a href="showUpdate?id=${car.id}">edit</a></td>
</tr>
</c:forEach>
</table>
<a href="showCreate">Add Car</a>
</body>
</html>