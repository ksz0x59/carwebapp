<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page isELIgnored="false"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Update Car</title>
</head>
<body>

	<form action="updateCar" method="post">
		<pre>
Id: <input type="text" name="id" value="${car.id}" readonly />
Vin: <input type="text" name="vin" value="${car.vin}" />
Brand: <input type="text" name="brand" value="${car.brand}" />
Model: <input type="text" name="model" value="${car.type}"/>
Type: <select name="type">
		<option ${car.type == 'Sedan' ? 'selected' : ''}>Sedan</option>
		<option ${car.type == 'Kombi' ? 'selected' : ''}>Kombi</option>
		<option ${car.type == 'Suv' ? 'selected' : ''}>Suv</option>
		<option ${car.type == 'Hatchback' ? 'selected' : ''}>Hatchback</option>
	  </select>
Year: <input type="text" name="year" value="${car.year}"/>
<input type="submit" value="save" />
</pre>
	</form>

</body>
</html>
