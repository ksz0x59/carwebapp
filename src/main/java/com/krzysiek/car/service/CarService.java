package com.krzysiek.car.service;

import java.util.List;

import com.krzysiek.car.entities.Car;

public interface CarService {

	Car saveCar(Car car);

	Car updateCar(Car car);

	void deleteCar(Car car);

	Car getCarById(int id);

	List<Car> getAllCars();
}
