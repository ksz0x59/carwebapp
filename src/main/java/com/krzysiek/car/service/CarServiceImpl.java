package com.krzysiek.car.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.krzysiek.car.entities.Car;
import com.krzysiek.car.repos.CarRepository;

@Service
public class CarServiceImpl implements CarService {

	@Autowired
	private CarRepository repository;

	public CarRepository getRepository() {
		return repository;
	}

	public void setRepository(CarRepository repository) {
		this.repository = repository;
	}

	@Override
	public Car saveCar(Car car) {
		return repository.save(car);
	}

	@Override
	public Car updateCar(Car car) {
		return repository.save(car);
	}

	@Override
	public void deleteCar(Car car) {
		repository.delete(car);
		
	}

	@Override
	public Car getCarById(int id) {
		return repository.findOne(id);
	}

	@Override
	public List<Car> getAllCars() {
		return repository.findAll();
	}

}
