package com.krzysiek.car.entities;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
public class Car {

	@Id
	private int id;
	private String vin;
	private String brand;
	private String model;
	private String type;
	@DateTimeFormat(pattern = "MM/dd/yyyy")
	private Date year;
	
	public int getId() {
		return id;
	}
	
	public void setId(int id) {
		this.id = id;
	}
	
	public String getVin() {
		return vin;
	}
	
	public void setVin(String vin) {
		this.vin = vin;
	}
	
	public String getBrand() {
		return brand;
	}
	
	public void setBrand(String brand) {
		this.brand = brand;
	}
	
	public String getModel() {
		return model;
	}
	
	public void setModel(String model) {
		this.model = model;
	}
	
	public String getType() {
		return type;
	}
	
	public void setType(String type) {
		this.type = type;
	}
	
	public Date getYear() {
		return year;
	}
	
	public void setYear(Date year) {
		this.year = year;
	}
	
	@Override
	public String toString() {
		return "Car [id=" + id + ", vin=" + vin + ", brand=" + brand + ", model=" + model + ", type=" + type + ", year="
				+ year + "]";
	}

}
