package com.krzysiek.car.repos;

import org.springframework.data.jpa.repository.JpaRepository;

import com.krzysiek.car.entities.Car;

public interface CarRepository extends JpaRepository<Car, Integer> {

}
