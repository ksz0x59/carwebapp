package com.krzysiek.car;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarwebApplication {

	public static void main(String[] args) {
		SpringApplication.run(CarwebApplication.class, args);
	}
}
