package com.krzysiek.car.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.krzysiek.car.entities.Car;
import com.krzysiek.car.service.CarService;

@Controller
public class CarController {

	@Autowired
	CarService service;

	@RequestMapping("/showCreate")
	public String showCreate() {
		return "createCar";
	}

	@RequestMapping("/saveCar")
	public String saveCar(@ModelAttribute("Car") Car Car, ModelMap modelMap) {
		Car CarSaved = service.saveCar(Car);
		String msg = "Car saved with id: " + CarSaved.getId();
		modelMap.addAttribute("msg", msg);
		return "createCar";
	}

	@RequestMapping("/displayCars")
	public String displayCars(ModelMap modelMap) {
		List<Car> cars = service.getAllCars();
		modelMap.addAttribute("cars", cars);
		return "displayCars";
	}

	@RequestMapping("deleteCar")
	public String deleteCar(@RequestParam("id") int id, ModelMap modelMap) {
		Car Car = new Car();
		Car.setId(id);
		service.deleteCar(Car);
		List<Car> cars = service.getAllCars();
		modelMap.addAttribute("cars", cars);
		return "displayCars";
	}

	@RequestMapping("/showUpdate")
	public String showUpdate(@RequestParam("id") int id, ModelMap modelMap) {
		Car car = service.getCarById(id);
		modelMap.addAttribute("car", car);
		return "updateCar";
	}

	@RequestMapping("/updateCar")
	public String updateCar(@ModelAttribute("Car") Car car, ModelMap modelMap) {
		service.updateCar(car);
		List<Car> cars = service.getAllCars();
		modelMap.addAttribute("cars", cars);
		return "displayCars";
	}

}
