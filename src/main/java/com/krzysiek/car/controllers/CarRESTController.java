package com.krzysiek.car.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.krzysiek.car.entities.Car;
import com.krzysiek.car.repos.CarRepository;

@RestController
@RequestMapping("/cars")
public class CarRESTController {

	@Autowired
	CarRepository carRepository;

	@GetMapping
	public List<Car> getCars() {
		return carRepository.findAll();

	}

	@PostMapping
	public Car createCar(@RequestBody Car car) {
		return carRepository.save(car);
	}

	@PutMapping
	public Car updateCar(@RequestBody Car car) {
		return carRepository.save(car);

	}

	@DeleteMapping("/{id}")
	public void deleteCar(@PathVariable("id") int id) {
		carRepository.delete(id);
	}
	
	@GetMapping("/{id}")
	public Car getCar(@PathVariable("id") int id) {
		return carRepository.findOne(id);
		
	}

}











