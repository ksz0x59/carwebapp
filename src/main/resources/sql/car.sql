use projectdb

create table car(
id int PRIMARY KEY,
vin varchar(20),
brand varchar(20),
model varchar(20),
type varchar(20),
year DATE
)

select * from car

drop table car